DROP PROCEDURE IF EXISTS CheckSchedules;

DELIMITER $$

CREATE PROCEDURE CheckSchedules
(IN SchID VARCHAR(10) , IN MvID VARCHAR(10),
OUT CinID VARCHAR(10) , OUT TimeSch DateTime)
BEGIN

	SELECT CinemaID , TimeSchedules

	INTO CinID , TimeSch

	FROM MovieSchedules
	WHERE
		ScheduleID = SchID and
		MovieID = MvID;
END $$
DELIMITER ;

