CREATE VIEW Registrationview AS
	
	SELECT  MS.ScheduleID , MS.SchedulesName,
		M.MovieName , C.CinemaID,
		MS.TimeSchedules

	FROM MovieSchedules AS MS
	LEFT JOIN (Movies AS M , Cinema AS C)
	ON
		MS.MovieID=M.MovieID and 
		MS.CinemaID=C.CinemaID;
