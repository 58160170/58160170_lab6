CREATE VIEW Regist AS

	SELECT  R.RegistID , MS. ScheduleID , MS.SchedulesName,
		C.CustomerName , C.CustomerSurname
	
	FROM Register AS R
	LEFT JOIN (MovieSchedules AS MS , Customers AS C)

	ON 
		R.ScheduleID=MS.ScheduleID and 
		R.CustomerID=C.CustomerID;
