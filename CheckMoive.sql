DROP PROCEDURE IF EXISTS CheckMoive;

DELIMITER $$
CREATE PROCEDURE CheckMoive
(IN RegID VARCHAR(10) , OUT CustName VARCHAR(50) , OUT CustSurname VARCHAR(50))
BEGIN

	SELECT CustomerName , CustomerSurname
	INTO CustName , CustSurname
	FROM Regist
	WHERE
		RegistID = RegID;

END $$
DELIMITER ; 
